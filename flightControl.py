#!/usr/bin/python3

import socket;
import datetime;
import cgi;

colors = { '-':'#FFFFFF', 'N':'#C0C0C0', 'A':'#FFFF00', 'W':'#4080E0', 'S':'#00FF00', 'E':'#FF0000'}

class StatusServer : 
    def __init__(self, address, port) :
        self.name = address;
        self.address = address;
        self.port = port;
        self.statuses = dict();
        self.pilotStatus = None;

        
    def connect(self) :
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((self.address, self.port));

        self.sockFile = self.sock.makefile();
        self.sock.settimeout(0.300);

    def getName(self) :
        self.sock.send(b'systemID\n');
        ret = self.getResponse();
        bits = ret.split('=');
        self.name = bits[-1].strip().strip();

    def close(self) : 
        self.sock.send(b'close\n');
        self.sock.close();
        
    def readStatuses(self) :
        self.sock.send(b'pilotStatus\n');
        ret = self.getResponse();
        
        bits = ret.split('=');
        self.pilotStatus = bits[1].strip();
        
        self.sock.send(b'getStatuses\n');
        ret = self.getResponse();
        
        lines = ret.split('\n');
        self.statuses = dict();
        for line in lines :
            parts = line.split(',');
            if parts[0] != "STAT" :
                continue;

            id = None;
            for part in parts[1:] :
                bits = part.split('=');
                if bits[0] == "ID" : 
                    id = bits[1];
                    if id.startswith("null") : 
                        id = id[4:];
                    self.statuses[id] = dict();
                else :
                    self.statuses[id][bits[0]] = bits[1];
                    
        
        
    def getResponse(self) :
        bufSize=1024*1024;
        buffer = ''
        data = True
        try :
            while data :    
                data = self.sock.recv(bufSize)
                buffer += data.decode();
        except socket.timeout :
            pass;
        
        return buffer;

    def syncClock(self) :
        self.sock.send(b'syncClock\n');
        self.sock.settimeout(15.000); # needs much longer
        ret = self.getResponse();
        bits = ret.split('=');
        offset = bits[-1].strip().strip();	
        self.sock.settimeout(0.300);

        return offset;


print("Content-type: text/html\n\n<html><body><h1>ImageProc-W7XPilot FlightControl</h1>");

form = cgi.FieldStorage()
showForced = False;
if "showForced" in form :
	showForced = form['showForced'];
if showForced :
	print("<P><B>Showing forced shots.</B> <A HREF='flightControl.py'>Don't show forced</A></P>");
else : 
	print("<P>Not showing forced shots. <A HREF='flightControl.py?showForced=True'>show forced</A></P>");

servers = [
    StatusServer("pc-e3-qsk-1.ipp-hgw.mpg.de", 9752), # QSK06-USB_HR4000
    StatusServer("pc-e3-qsk-1.ipp-hgw.mpg.de", 9753), # QSK06-USB_USB4000
    StatusServer("pc-e3-qsk-3.ipp-hgw.mpg.de", 9752),  # QSK06-USB_Avaspec
    StatusServer("pc-e3-qsk-3.ipp-hgw.mpg.de", 9754), # QSK06-PICamSpexM750
    StatusServer("pc-e3-qsk-4.ipp-hgw.mpg.de", 9752), # QSK06-PICamSpexM750-UV
    StatusServer("pc-e4-qss-24.ipp-hgw.mpg.de", 9752), # pc-e4-qss-24 QSS-PICamTest
    StatusServer("pc-e4-qss-25.ipp-hgw.mpg.de", 9752), # pc-e4-qss-35, QSS-USB_USB650
    StatusServer("pc-e4-qss-28.ipp-hgw.mpg.de", 9752), # pc-e4-qss-38, QSS-USB_USB650
    StatusServer("pc-e4-qss-32.ipp-hgw.mpg.de", 9752), # pc-e4-qss-32, QSS-USB_USB650
    StatusServer("pc-e4-qss-36.ipp-hgw.mpg.de", 9752), # pc-e4-qss-36, QSS-USB_USB650
    StatusServer("pc-e4-qss-108.ipp-hgw.mpg.de", 9752), # pc-e4-qss-108 Erhui's
    StatusServer("pc-e4-qri-2.ipp-hgw.mpg.de", 9752), # QRI-AEQ21
    StatusServer("pc-e4-qri-3.ipp-hgw.mpg.de", 9752), # QRI-AEF30
    StatusServer("pc-e4-qsr-2.ipp-hgw.mpg.de", 9752), # QSR-Zyla
];

progPrefix = datetime.date.today().strftime("%Y%m%d") + ".";
#progPrefix = None;

if "syncClocks" in form :
    for server in servers :
        try:
            server.connect()
            server.getName()
            print("<p>%s: " % (server.name), end='');
            result = server.syncClock()
            print("%s</p>" % (result));
            server.close();
        except Exception :
            print("<p>Cannot connect to server for "+server.name+" at "+server.address + "</p>");
            server.pilotStatus = 'E';
    exit();


allProgs = list();
for server in servers :
    try :
        server.connect()
        server.getName()
        server.readStatuses()
        server.close();

        if progPrefix is not None :
            for progID in server.statuses.keys() :
                if progID.startswith(progPrefix) :
                    allProgs.append(progID[len(progPrefix):]);
        else : 
            allProgs += list(server.statuses.keys());

#        print("<p>%s: Read OK</p>" % server.name);
        
#    except ConnectionRefusedError :
    except Exception :
        print("<p>Cannot connect to server for "+server.name+" at "+server.address + "</p>");
        server.pilotStatus = 'E';


allProgs = list(set(allProgs));
allProgs.sort();

if not showForced :
    allProgs = [item for item in allProgs if not item.startswith("F")]

htmlOut = "<table border=1 cellspacing=0 cellpadding=0>";
htmlOut += "<tr><td>Program:</td><td>Pilot</td>";
for progID in allProgs :
    htmlOut += "<td colspan=4>" + progID + "</td>";
htmlOut += "</tr>\n";

for server in servers :

    if server.pilotStatus is None :
        code = '-';
    else :
        code = server.pilotStatus;        

        
    htmlOut += "<tr><td bgcolor='"+colors[code]+"'>" + server.name;
    if server.name != server.address :
        htmlOut += " <font size=1>(" + server.address + ":" + str(server.port) + ")</font>";
    htmlOut += "</td><td bgcolor='"+colors[code]+"'>" + code + "</td>"; 
    
    for prog in allProgs :
        progID = progPrefix + prog;
        if progID not in server.statuses :
            htmlOut += "<td>-</td><td>-</td><td>-</td><td>&nbsp;</td>";
            continue;
        
        for stage in ['AQ', 'SV', 'PR'] :
            code = server.statuses[progID][stage];
            htmlOut += "<td bgcolor='"+colors[code]+"'>" + code + "</td>";
        htmlOut += "<td></td>";

    htmlOut += "</tr>\n";

htmlOut += "</table>\n<p>Boxes: Acquire, Save, Process; -=Unknown, N=Not Started, W=Waiting, A=Active, S=Success, E=Error</p>";

print(htmlOut + "</body></html>");
