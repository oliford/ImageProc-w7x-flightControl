package imageProc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalQueries;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonStreamParser;

import imageProc.PilotComm.ProgramStatus;
import imageProc.PilotComm.StepStatus;

public class FlightControl extends HttpServlet {
	
	private static String pilotListFile = "/var/lib/tomcat9/work/pilotList.txt";		
	
	private static String thisServlet = "FlightControl";
	
	private static SimpleDateFormat dateFmt = new SimpleDateFormat("YYYYMMdd");
	
	private static int serverPort = 9752;
	private static int listenAcceptTimeoutMS = 1000;
	
	/** User specific settings, stored as json in cookie */
	public static class UserViewConfig {
		public String progFilter;
		
		public String idFilter;
		
		/** Show program IDs with _NBI */
		public boolean showNBIPrograms = false;
		
		/** put programs with IDs like _tHH:MM into their proper program ID */
		public boolean mergeTimeTriggers = true;
		
		/** Show only the last this many programs */
		public int showLastNOnly = 10;
	}
	
	/** Prepared text commands */
	private final static String[] preparedCommands = { 
			"terminate",
			"kill",
			"reboot",
			"Power get", 
			"Power on", 
			"Power off", 
			"ModuleCommand AcquisitionDevice open", 
			"ModuleCommand AcquisitionDevice close" 
	};

	
	private UserViewConfig config;
	
	/** Broadcasted timestamps from GERI, so we don't repeat them */
	private ArrayList<Long> broadcastedTimestamps = new ArrayList<>();
	
	private static HashMap<Character,String> colors = new HashMap<>();
	static { 
		colors.put('?', "#C08080");
		colors.put('-', "#C08080");
		colors.put('N', "#C0C0C0");
		colors.put('A', "#FFFF00");
		colors.put('W', "#4080E0");
		colors.put('S', "#00FF00");
		colors.put('E', "#FF0000");
	}
	
	ArrayList<PilotComm> pilotComms = new ArrayList<>();
	
	public static class W7XProgram {
		public String id;
		public String name;
		public String description;
		public long from;
		public long upto;
	}
	
	public static class W7XProgramsList {
		public W7XProgramsList() {
			programs = new W7XProgram[0];
		}
		
		public W7XProgram[] programs;
	}
	
	/** List of program start nanotimes to map missing program numbers */
	private W7XProgramsList w7xPrograms = new W7XProgramsList();
		
	private double shotLength = -1;
	
	private Timer reconnectTimer;
	
	private Timer programsTimer;
	
	class Listener implements Runnable {
		private boolean death = false;
		public ServerSocket listeningSocket;
		private FlightControl ctrl;
		
		public Listener(FlightControl ctrl) {
			this.ctrl = ctrl;
		}

		@Override
		public void run() {
			try {
				listeningSocket = new ServerSocket(serverPort, 10);

				listeningSocket.setSoTimeout(listenAcceptTimeoutMS);
				
				log("Listening for inbound connections on " + listeningSocket.toString());
				
				while(!death) {
					Socket inboundSocket;
					try {
						inboundSocket = listeningSocket.accept();

					}catch(SocketTimeoutException err) {
						continue; //just cycle
					}

					try {
						if(death) 
							break;
					
						log("Accepted inbound connection from " + inboundSocket.toString());
						
						BufferedReader in = new BufferedReader(new InputStreamReader(inboundSocket.getInputStream()));
						
						//see if we can see who it is first and use an existing connection
						inboundSocket.setSoTimeout(3000);
						
						PilotComm existingComm = null;
						int port = -1;
						try {
							String response = in.readLine();
							log("Inbound said " + response);
							
							String parts[] = response.split("=",2);
							if(!parts[0].equals("SYSID"))
								throw new RuntimeException("First communication was not SYSID but: " + response);
							String id = parts[1];
							log("Inbound connection has SYSID = " + id);
							
							response = in.readLine();
							log("Inbound said " + response);
							parts = response.split("=",2);
							if(!parts[0].equals("PORT"))
								throw new RuntimeException("Second communication was not PORT but: " + response);
							port = Integer.parseInt(parts[1]);
							log("Inbound connection has PORT = " + port);
							
							
							//see if we already have this one
							for(PilotComm comm : pilotComms){
								if(id.equals(comm.getSystemID())) {
									existingComm = comm;
									break;
								}
							}
							
							//if existing and that one is not already connected
							if(existingComm != null && !existingComm.isConnected()) {
								log("Using existing connection " + existingComm.toString());
								existingComm.start(inboundSocket, in, port);								
							}
							
						}catch(RuntimeException err) {
							log("Error getting ID from inbound connection: " + err.getMessage());
						}
						if(existingComm == null) {
							log("No existing comm or no ID, making a new one");
							pilotComms.add(new PilotComm(ctrl, inboundSocket, in, port));
						}
						
					}catch(RuntimeException err) {
						log("Error on new inbound connection: " + err.getMessage());
					}
				}
				log("Listening stopped");
				
				
			} catch (IOException e) {
				log("Error in listening thread: " + e.getMessage());
				e.printStackTrace();
			}finally {
				if(listeningSocket != null) {
					try {
						listeningSocket.close();
					}catch(Exception err) {
						log("Can't close listener", err);
					}
				}
			}
		}
		
		public void stop() {
			death = true;
			if(listenThread != null)
				listenThread.interrupt();
			if(listeningSocket != null) {
				//close from this thread too, just to try really hard to kill it
				try {
					listeningSocket.close();
				}catch(Exception err) {
					log("Can't close listener", err);
				}
			}
		}
		
	}
	
	private Listener listener;
	private Thread listenThread; 
	
	public FlightControl() {
		
		try (FileReader fileReader = new FileReader(new File(pilotListFile))) {
			try (BufferedReader input = new BufferedReader(fileReader)) {
				Stream<String> lines = input.lines();
				
				for(String line : lines.toList()) {
					String parts[] = line.split(":", 3);
					String host = parts[0];
					String id = (parts.length>2) ? parts[2] : null;
					
					int port = 9752;
					if(parts.length > 1) {
						try {
							port = Integer.parseInt(parts[1]);
						}catch(NumberFormatException err) { }
					}	
						
					pilotComms.add(new PilotComm(this, host, port, id));
				}
			}
		} catch (IOException err) {
			err.printStackTrace();
			
			pilotComms.add(new PilotComm(this, "pc-e3-qsk-6", 9752, "ILS_Red"));
			
			savePilotList();
		}
		
		reconnectTimer = new Timer("reconnects");
		reconnectTimer.schedule(new PeriodicReconnectTask(), 3000, 3000);
		
		programsTimer = new Timer("reconnects");
		programsTimer.schedule(new GetProgramsTask(), 3000, 3000);
		
		listener = new Listener(this);
		listenThread = new Thread(listener);
		listenThread.start();
		
	}
	
	class PeriodicReconnectTask extends TimerTask {

		@Override
		public void run() {
			for(PilotComm comm : pilotComms) {
				if(!comm.isConnected() && comm.periodicReconnect) {
					comm.logWrite("Autoreconnect");
					comm.start();
				}
			}
		}
		
	}
	
	public static String programsJsonURL = "http://archive-webapi.ipp-hgw.mpg.de/programs.json";
	
	class GetProgramsTask extends TimerTask {

		@Override
		public void run() {
			try {
			
				StringBuilder result = new StringBuilder();
				URL url = new URL(programsJsonURL);
				
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setConnectTimeout(5000);
				conn.setReadTimeout(5000);
				conn.setRequestMethod("GET");
				try (BufferedReader reader = new BufferedReader(
				            new InputStreamReader(conn.getInputStream()))) {
				    for (String line; (line = reader.readLine()) != null; ) {
				        result.append(line);
				    }
				}
				
				Gson gson = new Gson();
				w7xPrograms = gson.fromJson(result.toString(), W7XProgramsList.class);

			}catch(IOException err){
				Logger.getLogger("FlightControl").log(Level.WARNING, "Unable to fetch/process programs list", err); 
			}
		}		
	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		response.setContentType("text/html;charset=UTF-8");
		
		//common output for all pages
		PrintWriter out = response.getWriter();
		try {
			config = configFromCookie(request);
			
			boolean updateConfig = false;
			
			if(config.progFilter == null || config.progFilter.trim().length() == 0) {
				config.progFilter = "today";
				updateConfig = true;
			}
			
			if(config.idFilter == null || config.idFilter.trim().length() == 0) {
				config.idFilter = "all";
				updateConfig = true;
			}
			
			//apply modified settings
			if(request.getParameter("progFilter") != null){
				config.progFilter = request.getParameter("progFilter");
				updateConfig = true;
			}
			
			if(request.getParameter("idFilter") != null){
				config.idFilter = request.getParameter("idFilter");
				updateConfig = true;
			}
			
			if(config.progFilter != null && config.progFilter.equalsIgnoreCase("today")) {
				config.progFilter = dateFmt.format(new Date()) + ".*";			
				updateConfig = true;
			}
			
			if(config.idFilter != null ) {
				String l = config.idFilter;
				if(!l.equals(config.idFilter)) {
					config.idFilter = l;
					updateConfig = true;
				}
			}
			
			if(config.progFilter != null) {
				String l = config.progFilter;
				if(!l.equals(config.progFilter)) {
					config.progFilter = l;
					updateConfig = true;
				}
			}
			
			if(request.getParameter("numProgs") != null){
				config.showLastNOnly = Integer.parseInt(request.getParameter("numProgs"));
				updateConfig = true;
			}			
						
			if(updateConfig)
				response.addCookie(cookieFromConfig(config));

			String action = request.getParameter("action");
			if(action != null && action.equals("javaLog") && request.getParameter("download") != null){
				javaLog(request, response, true);				
			}
			
			out.println("<!DOCTYPE html>"
					+ "<html><head>"
					+ "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>"
					+ ((action == null || action.equals("home") || action.equals("setShotLength") || action.equals("setNumProgs")
							 || action.equals("open") || action.equals("close")) 
							? "<meta http-equiv=\"refresh\" content=\"5; url=" + thisServlet + "\" />" : "")
					+ "<link rel=\"stylesheet\" href=\"static/gicons.css\">"
					+ "<title>ImageProc acquisition systems control</title></head>"
					+ "<body>"
					+ "<h1><a href='"+thisServlet +"'><i class=\"material-icons\" style=\"font-size:36px;color:#000080;\">flight</i></a>"
					  + " ImageProc acquisition control</h1>"
					);
			
			
			if(action != null && action.equals("add")){
				add(request, response);				return;
				
			}else if(action != null && action.equals("command")){
				command(request, response);
				return;
			}else if(action != null && action.equals("commandMultiPage")){
				commandMultiPage(request, response);
				return;
			}else if(action != null && action.equals("commandMulti")){
				commandMulti(request, response);	
				return;	
			}else if(action != null && action.equals("open")){
				open(request, response);
				return;
			}else if(action != null && action.equals("remove")){
				remove(request, response);
				return;
			}else if(action != null && action.equals("rename")){
				rename(request, response);
				return;
			}else if(action != null && action.equals("close")){
				close(request, response);
				return;
			}else if(action != null && action.equals("commsLog")){
				commsLog(request, response);
				return;
			}else if(action != null && action.equals("pilotLog")){
				pilotLog(request, response);
				return;
			}else if(action != null && action.equals("javaLog")){
				javaLog(request, response, false);
				return;
			}else if(action != null && action.equals("info")){
				pilotInfo(request, response);
				return;
			}else if(action != null && action.equals("forceMultiPage")){
				forceMultiPage(request, response);
				return;
			}else if(action != null && action.equals("connPage")){
				connectionsPage(request, response);
				return;
			}else if(action != null && action.equals("forceMulti")){
				forceMulti(request, response);	
				return;		
			}else if(action != null && action.equals("setShotLength")) {
				setShotLength(request, response);	
				
			}else {
				out.println("<p>Unrecognised action '"+action+"'</p>");
			}
			
			mainPage(request, response);
			
			
		}catch(Throwable err){
			out.println("<h3>EXCEPTION: " + err.getMessage() + "</h3><p><pre>");
			err.printStackTrace(System.out);
			out.println("</pre></p>");
			
		} finally {
			out.println("</body>");
			out.println("</html>");
			
			out.close();  // Always close the output writer
		}	
	}
	
	private UserViewConfig configFromCookie(HttpServletRequest request) {
		try {
			for(Cookie c : request.getCookies()) {
				if(c.getName().equals("config")) {
					String jsonConf = c.getValue();
					
					jsonConf = URLDecoder.decode(jsonConf, StandardCharsets.UTF_8);
					
					Gson gson =new Gson(); 
					return gson.fromJson(jsonConf, UserViewConfig.class);					
				}
			}
		}catch(RuntimeException err) {
			Logger.getLogger("FlightControl").log(Level.WARNING, "Error getting config from cookie: "+ err.getMessage(), err);
		}
			
		return new UserViewConfig();		
	}
	
	private Cookie cookieFromConfig(UserViewConfig config) {
		Gson gson = new Gson();
		String jsonStr = gson.toJson(config);
		jsonStr = URLEncoder.encode(jsonStr, StandardCharsets.UTF_8);
		//jsonStr = Base64.getEncoder().withoutPadding().encode(jsonStr.getBytes());
		Cookie c = new Cookie("config", jsonStr);
		
		return c;		
	}
		
	private void setShotLength(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
			
		try {
			double newLength = Double.parseDouble(request.getParameter("shotLength"));
			shotLength = newLength;
		}catch(NumberFormatException err) {
			out.println("<p>ERROR:" + err.getMessage() + "</p>");
			return;
		}
				
		out.println("<p>Setting shot length to " + shotLength + "</p>");
		for(PilotComm comm : pilotComms) {
			if(comm.isConnected()) {
				out.println("<p>"+comm.getSystemID()+"</p>");
				comm.setConfigParam("shotLength", Double.toString(shotLength));
			}
		}		
			
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void mainPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		
		statusTable(request, response);
		
		out.println("<p><a href='"+thisServlet+"?action=commandMultiPage'><i class=\"material-icons\" style=\"font-size:36px;color:#008000;\">thumb_up</i> Send command</a> - Send a text command to multiple pilots");
		
		out.println("<p><a href='"+thisServlet+"?action=connPage'><i class=\"material-icons\" style=\"font-size:36px;color:#008000;\">power</i> Connections</a> - Manage connections to the pilots");
		
		out.println("<p><a href='"+thisServlet+"?action=forceMultiPage'><i class=\"material-icons\" style=\"font-size:36px;color:#008000;\">thumb_up</i> Force program</a> - Force specific pilots to start a program at a specified time");
		
		out.println("<p><a href='"+thisServlet+"?action=forceMulti&progName=ForceAll&startTime=%2D60&all=true'><i class=\"material-icons\" style=\"font-size:36px;color:#800000;\">rocket_launch</i> Force ALL NOW!</a> - <font color='red'>Use if they are missing a shot. It won't harm ones that are already running.</font>");
	}
	
	private void forceMultiPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		out.println("<h2>Force program multiple pilots:</h2>"
				+ "<p><form action='FlightControl' method='post'>"
				+ "<input type='hidden' name='action' value='forceMulti'>");
		
		for(PilotComm comm : pilotComms)
			out.println("<input type='checkbox' name='"+comm.hashCode()+"' value='false'>"+comm.getSystemID()+"<br>");
		
		out.println("</p><p>Program name: <input type='text' name='progName'><br>"
				+ "Time: <input type='text' name='startTime'> (e.g. '+5s' for 5 seconds from now, or times e.g. '13:45' for local time or '13:45 UTC' etc.)<br>" 
				+ "<input type='submit' name='Start/Queue'></p>"
				+ "</form>");
	}
	
	private void forceMulti(HttpServletRequest request, HttpServletResponse response) throws IOException {
				
		PrintWriter out = response.getWriter();
		
		String id = request.getParameter("id");		
		
		String progName = request.getParameter("progName");
		if(progName.equals("ForceAll")){
			//for the 'force all' emergency forcing, we should make the string unique
			progName = LocalTime.now().format(DateTimeFormatter.ofPattern("YYYYDDmm"));
			progName += ".FA";
			progName += LocalTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
		}
		
		String startTimeStr = request.getParameter("startTime").trim();
		
		long startNanoTime = 0;
		
		if(startTimeStr.startsWith("+") || startTimeStr.startsWith("-") || startTimeStr.endsWith("s")){
			if(startTimeStr.endsWith("s"))
				startTimeStr = startTimeStr.substring(0, startTimeStr.length()-1);
			
			int secondsFromNow = Integer.parseInt(startTimeStr);
			long now = System.currentTimeMillis();
			startNanoTime = (now + secondsFromNow*1000L) * 1_000_000L;
			
		}else if(startTimeStr.contains(":")){ //looks like a time
			startNanoTime = decipherTime(startTimeStr) * 1_000_000L;
			
		}else{
			//assume it's actually a nano time
			startNanoTime = Long.parseLong(startTimeStr);
		}
		
		String startTimeFull = Instant.ofEpochMilli(startNanoTime / 1_000_000L).toString();
		
		out.println("<p>Forcing program '"+progName+"' at '"+startTimeFull+"' on pilots:<br>");
		
		String allParam = request.getParameter("all");
		boolean all = (allParam == null) ? false : Boolean.parseBoolean(allParam);
		
		for(PilotComm comm : pilotComms){
			String val = request.getParameter(Integer.toString(comm.hashCode()));
			if(all || (val != null && comm.isConnected())){
				comm.forceProgram(progName, Long.toString(startNanoTime));
				out.println(comm.getSystemID() + "<br>");
			}
		}
		out.println("</p>");
	
	}

	private void connectionsPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		
		out.println("<h2>Connections:</h2>"
				+ "<p><table border=1 cellspacing=0 cellpadding=0>"
				+ "<tr><td>ID</td><td>Host:port</td><td>Status</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
		for(PilotComm comm : pilotComms){
			String name = comm.getSystemID();
			int id = comm.hashCode();
			
			out.println("<tr><td>" + name + "</td>"
					+ "<td>" + comm.getHost() + ":" + comm.getPort() + "</td>"
					+ "<td>" + comm.getStatus() + "</td>"
					+ "<td><a href='" + thisServlet + "?action=open&id=" + id + "'>Open</a></td>"
					+ "<td><a href='" + thisServlet + "?action=close&id=" + id + "'>Close</a></td>"
					+ "<td><a href='" + thisServlet + "?action=commsLog&id=" + id + "'>Connection log</a></td>"
					+ "<td><a href='" + thisServlet + "?action=pilotLog&id=" + id + "'>Pilot log</a></td>"
					+ "<td><a href='" + thisServlet + "?action=info&id=" + id + "'>Info</a></td>"
					+ "</tr>");
		}
		out.println("</table></p>");
		
		out.println("<p>All: <a href='"+thisServlet+"?action=open&all=true" +  "'>Open</a>, "
						   + "<a href='"+thisServlet+"?action=close&all=true" +  "'>Close</a>, ");
		
		out.println("<h3>Add pilot:</h3>"
				+ "<p><form action='FlightControl' method='post'>"
				+ "<input type='hidden' name='action' value='add'>"
				+ "Host: <input type='text' name='host'>"
				+ "Port: <input type='text' name='port'>"
				+ "<input type='submit' name='Add'></p>"
				+ "</form>");
		
	}
	
	public static final Pattern timedIDPattern = Pattern.compile("([0-9]*)\\.([A-Za-z_].*)([0-9][0-9])([0-9][0-9])");
	
	private void statusTable(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		
		filterForms(out);
		
		out.println("<h2>Status</h2>");
				
		ArrayList<String> allProgs = new ArrayList<>();
		
		String progFilter = config.progFilter;
		if(progFilter.equalsIgnoreCase("today")) {
			progFilter = dateFmt.format(new Date()) + ".*";
		
		}else if(progFilter.equalsIgnoreCase("all")) {
			progFilter = null;
		}
		Pattern progFilterPattern = (progFilter != null) ? Pattern.compile(progFilter) : null; 
				
		String idFilter = config.idFilter;
		if(idFilter.equalsIgnoreCase("all"))
			idFilter = null;
		Pattern idFilterPattern = (idFilter != null) ? Pattern.compile(idFilter) : null; 
		
		
		for(PilotComm comm : pilotComms){			
			//change all .xxx_hhmm to the appropriate W7X shot, if there is one
			for(ProgramStatus stat : comm.getProgStats()){
				try {
					
					
					if(stat.altID != null)
						continue; //already done
					
					Matcher m = timedIDPattern.matcher(stat.id);
					
					if(!m.matches()) 
						continue; // isn't a xxxxx.t_hhmm type ID
									
					String todayStr = dateFmt.format(new Date());
					if(!m.group(1).equals(todayStr)) {
						continue; // isn't today
					}
					
					int hour = Integer.parseInt(m.group(3));
					int min = Integer.parseInt(m.group(4));
					
					
					LocalDateTime givenDate = LocalDateTime.of(LocalDate.now(), LocalTime.of(hour, min));
					
					long t = givenDate.toEpochSecond(ZoneOffset.UTC) * 1_000_000_000;
					
					for(W7XProgram p : w7xPrograms.programs) {
							
						if(p.from < (t + 60_000_000_000L) && p.upto > (t - 60_000_000_000L)) {
							stat.altID = p.id;
							out.println("<p>Assigned " + comm.getSystemID() + "." + stat.id + " to w7x program " + stat.altID + "</p>");
						}
					}
				}catch(Exception err) {
					Logger.getLogger("FlightControl").log(Level.WARNING, "Exception evaluation reassignment of times to shot numbers", err);
				}
			}
		}
	
	
	
		for(PilotComm comm : pilotComms){	
				
			if(idFilterPattern != null && !idFilterPattern.matcher(comm.getSystemID()).matches())
				continue;
			
			for(ProgramStatus stat : comm.getProgStats()){
				if((progFilter == null || progFilterPattern.matcher(stat.preferredID()).matches()) 
						&& !allProgs.contains(stat.preferredID()))
					allProgs.add(stat.preferredID());
			}
		}
		
		Collections.sort(allProgs, new Comparator<String>() {

			@Override
			public int compare(String s1, String s2) {
				//the numbers in it are usually a time, so use that and ignore the preceeding text
				s1 = s1.replaceAll("^[A-Za-z]*", "");
				s2 = s2.replaceAll("^[A-Za-z]*", "");
				return s1.compareTo(s2);
			}
			
		});
		
		//find the lastest program of today		
		int latestProg = 0;
		String dateStr = dateFmt.format(new Date());
		for(String prog : allProgs) {
			if(prog.startsWith(dateStr + ".")) {
				String parts[] = prog.split("\\.");
				try {
					int progNum = Integer.parseInt(parts[1]);
					if(progNum > latestProg)
						latestProg = progNum;
					
				}catch(Exception err) {					
				}
			}
		}
		out.println("<p>latestProg = " + latestProg + ", w7xProgs=" + w7xPrograms.programs.length + "</p>");
		
		out.println("<table border=1 cellspacing=0 cellpadding=0>"
			+ "<tr><td><a href='" + thisServlet + "?action=open&all=true'>"
			+ "<i class=\"material-icons\" style=\"font-size:16px;color:#008000;\">power</i>*</a></td>"
			+ "<td>Program:</td><td>Pilot</td>");
		
		boolean nonProgs = false;		
		for(String prog : allProgs) {
			if(!checkInLastN(prog, latestProg, dateStr))
				continue;
			
			if(!nonProgs && !prog.matches("20[0-9]*\\.[0-9]*")) {
				out.println("<td bgcolor='#000000'>&nbsp;</td>");
				nonProgs=true;
			}
			
			out.println("<td colspan=4>" + prog + "</td>");
		}
		
		out.println("</tr>");

		HashMap<String, int[]> progOKCount = new HashMap<String, int[]>(); 
		
		for(PilotComm comm : pilotComms){
			if(idFilterPattern != null && !idFilterPattern.matcher(comm.getSystemID()).matches())
				continue;
			
			char code;
			if(comm.getPilotStatus() == null)
				code = '-';
			else
				code = comm.getPilotStatus().code;        
		
			out.println("<tr><td bgcolor='"+colors.get(code)+"'>"
					+ "<a href='" + thisServlet + "?action=open&id=" + comm.hashCode() + "'>"
					+ "<i class=\"material-icons\" style=\"font-size:16px;color:#008000;\">power</i></a></td>"
					);
        
			out.println("<td bgcolor='"+colors.get(code)+"'>" + 
					"<a href='" + thisServlet + "?action=info&id=" + comm.hashCode() + "'>" + comm.getSystemID() + "</a>");
			String address = comm.getHost() + ":" + comm.getPort();
		    if(!comm.getSystemID().equals(address))
		        out.println(" <font size=1>(" + address + ")</font>");
		    out.println("</td><td bgcolor='"+colors.get(code)+"'>" + code + "</td>"); 
		    
		    ArrayList<ProgramStatus> progStats = comm.getProgStats();
		    
		    nonProgs = false;
		    for(String progName  : allProgs){
				if(!checkInLastN(progName, latestProg, dateStr))
					continue;
				
				if(!nonProgs && !progName.matches("20[0-9]*\\.[0-9]*")) {
					out.println("<td bgcolor='#000000'>&nbsp;</td>");
					nonProgs=true;
				}
				
		    	boolean found = false;
			    for(ProgramStatus stat : progStats){
			    	if(!stat.preferredID().equals(progName))
			    		continue;
			    	
			    	
			    	found = true;
			    	boolean allOk = true;
			    	for(StepStatus stepStat : new StepStatus[]{ stat.acquireStatus, stat.saveStatus, stat.processStatus }){
			    		out.println("<td bgcolor='"+colors.get(stepStat.code)+"'>" + stepStat.code + "</td>");
			    		allOk &= (stepStat.code == 'S');
			    	}
			    	
			    	int[] counts = progOKCount.get(progName);				
			    	
			    	if(counts == null)
			    		counts = new int[2];
			    	if(allOk)
			    		counts[0]++;
			    	counts[1]++;
			    	
			    	progOKCount.put(progName, counts);					
			    	
			    }
			    if(!found){
			    	out.println("<td>-</td><td>-</td><td>-</td>");
			    }
		    
		    	out.println("<td></td>");
		    }
		
		    out.println("</tr>\n");
		}


		//output total number of successful and attempted programs by pilots
		out.println("<tr><td>Count:</td><td colspan=2>Pilot</td>");
		nonProgs = false;
		for(String prog : allProgs){
			if(!checkInLastN(prog, latestProg, dateStr))
				continue;
			
			if(!nonProgs && !prog.matches("20[0-9]*\\.[0-9]*")) {
				out.println("<td bgcolor='#000000'>&nbsp;</td>");
				nonProgs=true;
			}
			
			int[] counts = progOKCount.get(prog);
			
	    	if(counts != null){
	    		String bgCol = (counts[0] == counts[1]) ? "#008000" : "#FFC0C0"; 
	    		out.println("<td colspan=3 bgcolor='"+bgCol+"'>" + counts[0] + " / " + counts[1] + "</td><td></td>");
	    	}else{
	    		out.println("<td colspan=3>-</td><td></td>");
	    	}
		}
		
		out.println("</tr>");

		 out.println("</table>\n<p>Boxes: Acquire, Save, Process; -/?=Unknown (not connected), N=Not Started, W=Waiting, A=Active, S=Success, E=Error</p>");

	}
	
	private boolean checkInLastN(String prog, int latestProg, String dateStr) {
		if(config.showLastNOnly <= 0)
			return true;
		
		if(prog.startsWith(dateStr + ".")) {
			String parts[] = prog.split("\\.");
			try {
				if(Integer.parseInt(parts[1]) <= (latestProg - config.showLastNOnly))
					return false;
			}catch(Exception err) {		    				
			}		    			
		}    	
		return true;
	}

	private void filterForms(PrintWriter out) {
		
		out.println("<h2>Fitlers</h2>"
				+ "<p><form action='FlightControl' method='post'>"
				+ "<input type='hidden' name='action' value='home'>"
				+ "Program ID: <input type='text' name='progFilter' value='" + config.progFilter +"'>"
				+ "<input type='submit' value='Set'> (regex) - "
				+ "<a href='"+thisServlet+"?action=home&progFilter=all'>All</a>, "
				+ "<a href='"+thisServlet+"?action=home&progFilter=" + URLEncoder.encode(dateFmt.format(new Date()) + ".*", StandardCharsets.UTF_8) + "'>Today</a>, "
				+ "<a href='"+thisServlet+"?action=home&progFilter=" + URLEncoder.encode(dateFmt.format(new Date()) + ".[0-9][0-9][0-9]", StandardCharsets.UTF_8) + "'>W7X Today</a>, "
				+ "<a href='"+thisServlet+"?action=home&progFilter=" + URLEncoder.encode(dateFmt.format(new Date()) + ".NBI.*", StandardCharsets.UTF_8) + "'>NBI Today</a>"
				+ "</form></p>"
				);
		

		
		out.println("<p><form action='FlightControl' method='post'>"
				+ "<input type='hidden' name='action' value='home'>"
				+ "System ID: <input type='text' name='idFilter' value='" + config.idFilter +"'>"
				+ "<input type='submit' value='Set'> (regex) - "
				+ "");
		
		out.println("<a href='"+thisServlet+"?action=home&idFilter=all'>All</a>");
		for(String diag : new String[]{ "Q", "QSK", "QSC", "QS[CK]", "QSS", "QSS70", "QRI", "CDX" }) {
			out.println("<a href='"+thisServlet+"?action=home&idFilter="+URLEncoder.encode(diag + ".*", StandardCharsets.UTF_8)+"'>" + diag + "</a>, ");
		}
		out.println("<a href='"+thisServlet+"?action=home&idFilter="+URLEncoder.encode("QSS70.*FZJ.*", StandardCharsets.UTF_8)+"'>QSS70-FZJ</a>, ");
		out.println("</form></p>");
				
		out.println("<h2>Settings/Commands</h2>"
				+ "<p><form action='FlightControl' method='post'>"
				+ "<input type='hidden' name='action' value='setShotLength'>"
				+ "<i class=\"material-icons\" style=\"font-size:36px;color:#008000;\">alarm</i> "
				   + "Shot length: <input type='text' name='shotLength' value='"+ shotLength  +"'> - "
				+ "<input type='submit' value='Set'>"
				+ "</form></p>"
				
				+ "<p><form action='FlightControl' method='post'>"
				+ "<input type='hidden' name='action' value='home'>"
				  + "Number of programs to show:<input type='text' name='numProgs' value='"+ config.showLastNOnly  +"'> - "
				+ "<input type='submit' value='Set'>"
				+ "</form></p>");
		
	}

	private void pilotInfo(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		int id = Integer.parseInt(request.getParameter("id"));
		PilotComm comm = getCommByHashcode(id);
		
		
		out.println("<h2>Pilot info</h2>"
				+ "<p><b>System ID:</b> " + comm.getSystemID()+ "</p>"
				+ "<p><b>Address:</b> " + comm.getHost() + ":" + comm.getPort() + "</p>"
				+ "<p><b>FlightControl hash:</b> " + comm.hashCode()+ "</p>"
				+ "<p><b>Status:</b> " + comm.getPilotStatus() + "</p>"
				+ "<p><b>Connection:</b> " + comm.getStatus()
				+ " <a href='" + thisServlet + "?action=open&id=" + id  + "'>Open</a>"
				+ " <a href='" + thisServlet + "?action=close&id=" + id  + "'>Close</a></p>"
				+ "<p><b>Edit:</b><form method='get' action='" + thisServlet + "'>"
				  + "<input type='hidden' name='action' value='rename'>"
				  + "<input type='hidden' name='id' value='" + id + "'>"
				  + "Host:<input type='text' name='host' value='" + comm.getHost() + "'>"
				  + "Port:<input type='text' name='port' value='" + comm.getPort() + "'>"
				  + "<input type='checkbox' name='reconnect' id='reconnect' value='true' " + (comm.periodicReconnect ? "checked" : "") + ">"
				  + "<label for='reconnect'>Periodic reconnect</label>"
				  + "&nbsp;&nbsp;<input type='submit' value='Set'></p>"
				+ "<p><a href='" + thisServlet + "?action=remove&id=" + id  + "'>Remove</a></p>"
				+ "<p><a href='" + thisServlet + "?action=commsLog&id=" + id + "'>Communication log</a></p>"
				+ "<p><a href='" + thisServlet + "?action=pilotLog&id=" + id + "'>Pilot log</a></p>"
				+ "<p><a href='" + thisServlet + "?action=javaLog&id=" + id + "'>Full Java log</a></p>");
		
		out.println("<h3>Force program:</h3>"
				+ "<p><form action='FlightControl' method='post'>"
				+ "<input type='hidden' name='action' value='forceMulti'>"
				+ "<input type='hidden' name='"+id+"' value='true'>"
				+ "Name: <input type='text' name='progName'>"
				+ "Time: <input type='text' name='startTime'> (e.g. '+5s' for 5 seconds from now, or times e.g. '13:45' for local time or '13:45 UTC' etc.)<br>" 
				+ "<input type='submit' name='Start/Queue'></p>"
				+ "</form>");
		
		out.println("<h3>Send command:</h3>"
				+ "<p><form action='FlightControl' method='post'>"
				+ "<input type='hidden' name='action' value='command'>"
				+ "<input type='hidden' name='id' value='"+id+"'>");

		for(int i=0; i < preparedCommands.length; i++) {
			out.println("<p><input type='radio' name='prepCommand' id='"+i+"' value='"+i+"'><label for='"+i+"'>"+preparedCommands[i]+"</label>");
			if(preparedCommands[i].equalsIgnoreCase("reboot"))
				out.println("<font color='red'><b>CAREFUL!</b></font>");
			out.println("</p>");
		}
	
		out.println("<p><input type='radio' name='prepCommand' id='' value=''><label for=''>Command:</label><input type='text' name='command'><br>"
				+ "<input type='submit' name='Send'></form></p>");
		
	}
	
	private void open(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		
		if(request.getParameter("all") != null){
			for(PilotComm comm : pilotComms){
				if(!comm.isConnected()){
					try{
						comm.start();
						out.println("<p>Opening connection to pilot '"+comm.getSystemID()+"'</p>");
					}catch(RuntimeException err){
						out.println("<p>Opening connection to pilot '"+comm.getSystemID()+"' failed: "+err.getMessage()+"</p>");
					}
				}
			}
			
		}else{
			int id = Integer.parseInt(request.getParameter("id"));
			PilotComm comm = getCommByHashcode(id);
		
			try{
				comm.start();
				out.println("<p>Opening connection to pilot '"+comm.getSystemID()+"'</p>");
			}catch(RuntimeException err){
				out.println("<p>Opening connection to pilot '"+comm.getSystemID()+"' failed: "+err.getMessage()+"</p>");
			}
		}
	}
	
	private void close(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		
		if(request.getParameter("all") != null){
			for(PilotComm comm : pilotComms){
				if(comm.isConnected()){
					comm.stop();
					out.println("<p>Closing connection to pilot '"+comm.getSystemID()+"'</p>");
				}
			}
			
		}else{
			int id = Integer.parseInt(request.getParameter("id"));
			PilotComm comm = getCommByHashcode(id);
		
			comm.stop();
			out.println("<p>Closing connection to pilot '"+comm.getSystemID()+"'</p>");
		}
		
		
	}

	private void add(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		String host = request.getParameter("host");
		int port = Integer.parseInt(request.getParameter("port"));
		
		out.println("<p>Adding pilot server on host '"+host+"' port "+port+"</p>");
		
		PilotComm comm = new PilotComm(this, host, port, null);
		
		pilotComms.add(comm);
		
		savePilotList();
	}
	
	private void remove(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		
		int id = Integer.parseInt(request.getParameter("id"));
		PilotComm comm = getCommByHashcode(id);
	
		out.println("<p>Closing connection to pilot '"+comm.getSystemID()+"'</p>");		
		comm.stop();
		
		out.println("<p>Removing entry for '"+comm.getSystemID()+"'</p>");	
		
		pilotComms.remove(comm);		
		
		savePilotList();
	}
	
	private void rename(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		
		int id = Integer.parseInt(request.getParameter("id"));
		PilotComm comm = getCommByHashcode(id);
		
		if(comm.isConnected())
			comm.stop();
	
		String host = request.getParameter("host");
		int port;
		try {
			port = Integer.parseInt(request.getParameter("port"));
		}catch(NumberFormatException err) {
			port = 9752;
		}
		
		try {
			comm.periodicReconnect = Boolean.parseBoolean(request.getParameter("reconnect"));
		}catch(NumberFormatException err) {
			comm.periodicReconnect = false;
		}
		
		comm.setHost(host);
		comm.setPort(port);
		String newID = "[" + host + ":" + port + "]";
		out.println("<p>Modified connection target of pilot '"+comm.getSystemID() + "'" + newID + "'</p>");		
		comm.setSystemID(newID);
		
		savePilotList();
	}
	
	private void savePilotList() {
		
        try (FileOutputStream fOut = new FileOutputStream(pilotListFile)) {
        	PrintStream ps = new PrintStream(fOut);
        	for(PilotComm comm : pilotComms)
        		ps.println(comm.getHost() + ":" + comm.getPort() + ":" + comm.getSystemID());        		
        	
        } catch (IOException err) {
            throw new RuntimeException(err);
        }
	}

	private void command(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		
		int id = Integer.parseInt(request.getParameter("id"));		
		PilotComm comm = getCommByHashcode(id);
		
		String prepCommand = request.getParameter("prepCommand");
		String command = (prepCommand != null && prepCommand.length() > 0) ? preparedCommands[Integer.parseInt(prepCommand)] : request.getParameter("command");
		if(command == null) {
			throw new RuntimeException("command is null");
		}
		
		comm.sendCommand(command);
		//unfortunately we can't really see the response. They have to look at the comms log
		out.println("<p>Sending '" + command + "' to " + comm.getSystemID() + "</p>"
				+ "<p>See <a href='" + thisServlet + "?action=commsLog&id=" + id + "'>connection log</a> for response</p>");
		
	}
	
	private void commandMultiPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		out.println("<h2>Send command to multiple pilots:</h2>"
				+ "<p><form action='FlightControl' method='post'>"
				+ "<input type='hidden' name='action' value='commandMulti'>");
		
		String idFilter = config.idFilter;
		if(idFilter.equalsIgnoreCase("all"))
			idFilter = null;
		Pattern idFilterPattern = (idFilter != null) ? Pattern.compile(idFilter) : null; 
		
		
		for(PilotComm comm : pilotComms) {
			boolean check = idFilterPattern != null && idFilterPattern.matcher(comm.getSystemID()).matches();
			
			out.println("<input type='checkbox' name='"+comm.hashCode()+"' " + (check ? "checked" : "") + ">"+comm.getSystemID()+"<br>");
		}
		
		out.println("</p>");
		
		for(int i=0; i < preparedCommands.length; i++) {
			out.println("<p><input type='radio' name='prepCommand' id='"+i+"' value='"+i+"'><label for='"+i+"'>"+preparedCommands[i]+"</label>");
			if(preparedCommands[i].equalsIgnoreCase("reboot"))
				out.println("<font color='red'><b>CAREFUL!</b></font>");
			out.println("</p>");
		}
		
		out.println("<p><input type='radio' name='prepCommand' id='' value=''><label for=''>Command:</label><input type='text' name='command'><br>"
				+ "<input type='submit' name='Send'></p>"
				+ "</form>");
	}
	
	private void commandMulti(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		PrintWriter out = response.getWriter();
		
		String id = request.getParameter("id");		
		
		String prepCommand = request.getParameter("prepCommand");
		String command = (prepCommand != null && prepCommand.length() > 0) ? preparedCommands[Integer.parseInt(prepCommand)] : request.getParameter("command");
				
		long startNanoTime = 0;
				
		out.println("<p>Sending command '"+command+"' to pilots:<br>");
		
		String allParam = request.getParameter("all");
		boolean all = (allParam == null) ? false : Boolean.parseBoolean(allParam);
		
		for(PilotComm comm : pilotComms){
			String val = request.getParameter(Integer.toString(comm.hashCode()));
			if(all || (val != null && comm.isConnected())){
				comm.sendCommand(command);
				out.println(comm.getSystemID() + "<br>");
			}
		}
		out.println("</p>");
		
	}
	
	private void commsLog(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		
		int id = Integer.parseInt(request.getParameter("id"));		
		PilotComm comm = getCommByHashcode(id);
				
		out.println("<p>Communication log for "+comm.getSystemID()+":<br><pre>" + comm.getLog() + "</pre></p>");
	}

	private void pilotLog(HttpServletRequest request, HttpServletResponse response) throws IOException {
			
			PrintWriter out = response.getWriter();
			
			int id = Integer.parseInt(request.getParameter("id"));		
			PilotComm comm = getCommByHashcode(id);
			
			comm.requestPilotLog();
			out.println("<p>Requested up to date pilot log, refresh page in a few seconds to see it.</p>");
			
			if(comm.getPilotLog() != null) {
				out.println("<p>Last pilot log downloaded from " + comm.getSystemID()+ " at " + comm.getPilotLogTime() + ":</p>"
						+ "<pre>"+comm.getPilotLog() + "</pre>");
			}else {
				out.println("<p>No previous pilot log available.</p>");
			}
					
			 
		}
	
	private void javaLog(HttpServletRequest request, HttpServletResponse response, boolean download) throws IOException {
		
		PrintWriter out = response.getWriter();
		
		int id = Integer.parseInt(request.getParameter("id"));		
		PilotComm comm = getCommByHashcode(id);
		
		if(download) {
			response.setContentType("application/xml;charset=UTF-8");
			response.setHeader("Content-Disposition", "attachment; filename=javaLog-" + comm.getSystemID() + ".xml");
			out.println(comm.getJavaLog());
			return;
		}
		
		String fetch = request.getParameter("fetch");
		if(fetch != null) {
			comm.requestJavaLog();
			out.println("<p>Requested up to date java log, <a href='" + thisServlet + "?action=javaLog&id=" + id + "'>check</a> in a few seconds to see it.</p>");
		}
		
		if(comm.getJavaLog() != null) {
			out.println("<p>Last Java log downloaded from " + comm.getSystemID()+ " at " + comm.getJavaLogTime() 
						+ ":<a href='" + thisServlet + "?action=javaLog&id=" + id + "&download=true'>Download</a>(view with e.g. Otros log viewer)</p>");
		}else {
			out.println("<p>No previous java log available.</p>");
		}
		
		out.println("<p><a href='" + thisServlet + "?action=javaLog&id=" + id + "&fetch=true'>Fetch current log</a></p>");
				
		 
	}
		
	private PilotComm getCommByHashcode(int hashcode) {
		for(PilotComm comm : pilotComms)
			if(comm.hashCode() == hashcode)
				return comm;
		
		throw new RuntimeException("Pilot with hashcode '" + hashcode + "' not in list.");
	}


	/** Attempt to decipher the user's time, based on today. 
	 * They may or may now have given a time zone.
	 * Assume local time if they don't specify
	 * 
	 */
	public long decipherTime(String str){
		
		String patterns[] = new String[]{ "HH:mm", "HH:mm:ss", "HH:mm z", "HH:mm:ss z" };
		
		for(String pattern : patterns) {
			DateTimeFormatter f = DateTimeFormatter.ofPattern(pattern);
			try{
				TemporalAccessor a = f.parse(str);
				
				LocalTime time = LocalTime.from(a);
				ZoneId zid = a.query(TemporalQueries.zoneId());
				if(zid == null){ //if no zone specified, use local
					zid = ZoneId.systemDefault();
				}
				
				LocalDate today = LocalDate.now();
				LocalDateTime dateTime = today.atTime(time);
				
				ZonedDateTime zdt = ZonedDateTime.of(dateTime, zid);
		
				Instant inst = zdt.toInstant();
		
				return inst.toEpochMilli();
				//System.out.println("'" + str + "' is in format' " + pattern + "': " + inst);
				
			}catch(DateTimeParseException err){ 
				//System.out.println("'" + str + "' is not in format' " + fStr + "': " + err.getMessage());
			}
		}
		
		throw new RuntimeException("Unable to pass date/time in any of the expected formats");
	}
	
	
	
	public void geriStateChange(int state, long timestamp) {
		if(broadcastedTimestamps.contains(timestamp))
			return; //duplicate
		
		broadcastedTimestamps.add(timestamp);
		
		//broadcast to all
		for(PilotComm comm : pilotComms)
			if(comm.isConnected())
				comm.sendGeriStateChange(state, timestamp);		
	}
	
	@Override
	public void destroy() {
		//kill the server and then all TCP connections of all pilots
		try {
			if(listener != null)		
				listener.stop();			
		}catch(Exception err) {
			log("Can't stop the listen socket", err);
		}
		
		for(PilotComm comm : pilotComms)
			comm.stop();
		
		super.destroy();
	}
	
}
