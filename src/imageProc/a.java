package imageProc;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.time.temporal.IsoFields;
import java.time.temporal.JulianFields;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalField;
import java.time.temporal.TemporalQueries;
import java.util.ArrayList;

public class a {
	public static void main(String[] args) {
		
		ArrayList<String> al = new ArrayList<String>();
		String b = "bbb";
		String b2 = "bb" + "b";
		
		al.add("aaa");
		al.add(b);
		al.add("ccc");

		System.out.println(al.contains("ddd"));
		System.out.println(al.contains(b2));
		
		//String str = "16:05:02";
		attemptFormat("16:05:02 CEST");
		attemptFormat("16:05 CEST");
		attemptFormat("16:05:02 UTC");
		attemptFormat("16:05 UTC");
		attemptFormat("16:05:02");
		attemptFormat("16:05");
	}
	
	public static void attemptFormat(String str){
		
		String patterns[] = new String[]{ "HH:mm", "HH:mm:ss", "HH:mm z", "HH:mm:ss z" };
		
		for(String pattern : patterns) {
			DateTimeFormatter f = DateTimeFormatter.ofPattern(pattern);
			try{
				TemporalAccessor a = f.parse(str);
				
				LocalTime time = LocalTime.from(a);
				ZoneId zid = a.query(TemporalQueries.zoneId());
				if(zid == null){
					zid = ZoneId.systemDefault();
				}
				
				LocalDate today = LocalDate.now();
				LocalDateTime dateTime = today.atTime(time);
				
				ZonedDateTime zdt = ZonedDateTime.of(dateTime, zid);
		
				Instant inst = zdt.toInstant();
		
				System.out.println("'" + str + "' is in format' " + pattern + "': " + inst);
				
			}catch(DateTimeParseException err){ 
				//System.out.println("'" + str + "' is not in format' " + fStr + "': " + err.getMessage());
			}
		}
	}
}
