package imageProc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ConcurrentLinkedQueue;

/** Communicator with pilot status. Hold socket and known information */ 
public class PilotComm implements Runnable {
	private final static SimpleDateFormat timestampFormat = new SimpleDateFormat("dd/MM HH:mm:ss");
	
	final static int socketReadTimeoutMS = 1000;
	
	public boolean periodicReconnect = false;
	
	public static enum StepStatus {
		/** Entry not set (or disconnect, so can't be sure) */
		UNKNOWN('?'),
		
		/** Step: Not yet begun.
		 * Pilot: Pilot is stopped */
		NOT_STARTED('N'), 

		/** Acquisition: Awaiting software/hardware trigger.
		 * Pilot: Started, waiting to start day control or waiting for a shot */
		WAITING('W'), 
		
		/** Step: Acquring/saving/processing etc is running.
		 * Pilot: In shot control */
		ACTIVE('A'),
		
		/** Step: Done ok 
		 * Pilot: Not used. */		
		SUCCESS('S'), 

		/** Step: Didn't complete 100% successfully.
		 * Pilot: Serious problem, given up entirely. */
		ERROR('E');
		
		public char code;
		private StepStatus(char code) { this.code = code; }
		
		public static StepStatus fromCode(char code){			
			for(StepStatus status : StepStatus.values()){
				if(status.code == code)
					return status;
			}
			throw new IllegalArgumentException("Unknown status code '"+code+"'");
		}
	};
	
	private StepStatus pilotStatus = StepStatus.UNKNOWN;
		
	public static class ProgramStatus {
		public String id;
		
		/** Alternate ID, e.g. w7x ID if the pilot sent a .t_ and we assigned it to a shot */
		public String altID = null;
		
		public String preferredID() {
			return altID != null ? altID : id;
		}
		
		public ProgramStatus(String programID) { this.id = programID; }
		
		public StepStatus acquireStatus = StepStatus.UNKNOWN;		
		public StepStatus saveStatus = StepStatus.UNKNOWN;
		public StepStatus processStatus = StepStatus.UNKNOWN;
		
		@Override
		public String toString() {
			return "STAT,ID=" + id  + ",ALTID=" + (altID == null ? "" : altID) 
					+ ",AQ=" + acquireStatus.code + ",SV=" + saveStatus.code + ",PR=" + processStatus.code;
		}
	};
	
	private ArrayList<ProgramStatus> progStats = new ArrayList<ProgramStatus>();
	private ConcurrentLinkedQueue<String> commands = new ConcurrentLinkedQueue<>();
	
	private String host;
	private int port;
	
	private String id = "null";
	
	private Socket socket;
	private BufferedReader in;
	
	private Thread thread;
	private boolean death;
	
	private String lastPilotLog;
	private Instant lastPilotLogTime;
	
	private String lastJavaLog;
	private Instant lastJavaLogTime;
	
	private StringBuffer log;
	
	private int geriState = -1;
	private long geriStateChanged = -1;
	
	private FlightControl ctrl;
	
	/** Create new outbound connection */
	public PilotComm(FlightControl ctrl, String host, int port, String id) {
		this.ctrl = ctrl;
		this.host = host;
		this.port = port;
		this.id = (id != null) ? id : ("[" + host + ":" + port + "]");
		this.socket = null; //not connected
		this.thread = null; //not started
		this.death = false; //not dying
		
		this.log = new StringBuffer(); 
	}
	
	/** Create comm to existing connection */
	public PilotComm(FlightControl ctrl, Socket socket, BufferedReader in, int port) {
		this.ctrl = ctrl;
		this.socket = socket;
		this.in = in;
		this.host = socket.getInetAddress().getHostAddress();
		this.port = port;
		this.id = "[" + host + ":" + port + "]";
		this.thread = null; //not started
		this.death = false; //not dying
		
		this.log = new StringBuffer();
		
		start(socket, in, port);
	}
	
	/** Start the thread and make an outbound connection to the pilot */
	public void start() {
		start(null, null, -1);
	}
			
	/**Start the thread with the given inbound connection from the pilot */
	public void start(Socket inboundSocket, BufferedReader in, int port){
		if(thread != null){
			if(thread.isAlive())
				throw new RuntimeException("Already started");
			thread = null;
		}
		
		logWrite("Starting thread");
		if(inboundSocket != null) {
			this.socket = inboundSocket;
			this.in = in;
			this.host = socket.getInetAddress().getHostAddress();
		}else {
			this.socket = null;
			this.in = null;			
		}
		if(port > 0)
			this.port = port;
		
		death = false;
		thread = new Thread(this);
		thread.start();
	}
	
	public void stop(){
		if(thread != null && thread.isAlive()){
			logWrite("Stopping thread");
			death = true;
			thread.interrupt();
		}
		thread = null;		
	}
	
	
	@Override
	public void run() {
		try {
			
			logWrite("Thread started");
			
			if(socket == null) {
				logWrite("Connecting to "+ host + ":" + port);
				socket = new Socket(host, port);
				
			}else {
				logWrite("Using inbound socket from "+ host + ":" + port);
			}
			socket.setSoTimeout(socketReadTimeoutMS);
			
			PrintWriter pw = new PrintWriter(socket.getOutputStream());
			if(in == null)
				in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			logWrite("Connected");
			
			//Get systemID
			pw.println("systemID");
			
			//Get pilot status
			pw.println("pilotStatus");
			
			//Get previous program statuses 
			pw.println("getStatuses");
			
			//Enable status change notifications
			pw.println("notifyChanges=true");
			
			pw.flush(); //send everything, then deal with all the responses
			
			//wait for notifications
			while(!death){
				try{
					String response = in.readLine();
									
					if(response == null) {
						logWrite("read = null, dropping connection");
						break;
					}
					
					if(response.startsWith("LOG=") || response.startsWith("JAVALOG=")){
						StringBuffer buf = new StringBuffer();
						
						if(response.startsWith("JAVALOG="))
							buf.append(response.substring(8));
						else
							buf.append(response.substring(4));
							
						do{
							try{
								String line = in.readLine();
							
								if(line == null || line.equals("<ENDLOG>"))
									break;
								
								buf.append(line + "\n");
							}catch(SocketTimeoutException timeoutErr){
								//send a ping and loop
								pw.println("PING");
								pw.flush();
							}
						}while(true);
						if(response.startsWith("JAVALOG")) {
							lastJavaLog = buf.toString();
							lastJavaLogTime = Instant.now();
							logWrite("Recieved java log. Size=" + lastJavaLog.length());						
						}else {
							lastPilotLog = buf.toString();
							lastPilotLogTime = Instant.now();
							logWrite("Recieved pilot log. Size=" + lastPilotLog.length());
						}
					}
					
					parseResponse(response);

					String command = commands.poll();
					if(command != null){
						pw.println(command);
						pw.flush();
					}
						
				}catch(SocketTimeoutException timeoutErr){
					//send a ping and loop
					pw.println("PING");
					pw.flush();
					
				}catch(RuntimeException err){
					logWrite("EXCEPTION: " + err.getMessage());
					err.printStackTrace();
				}
			}

			//try to hurl a close down the line to get the other end to realise we've disconnected
			try{ 
				pw.println("\nCLOSE");
			}catch(Throwable err){ } // ...don't care
			
		} catch(Throwable err){
			logWrite("ERROR: Exception in thread. Thread terminating: " + err.getMessage());
		}

		//set the pilot status back to unknown, since we can't be sure it hasn't changed
		pilotStatus = StepStatus.UNKNOWN;
		
		logWrite("Thread terminating");
		
		socket = null;
	}
	
	private void parseResponse(String response){
		String parts[] = response.split("=", 2);
		

		if(parts[0].equals("PONG")) {
			//nothing todo
			
		}else if(parts[0].equals("SYSID")){
			this.id = parts[1];
			logWrite("SystemID = " + id);
			
		}else if(parts[0].equals("PILOT")){
			this.pilotStatus = StepStatus.fromCode(parts[1].trim().charAt(0));
			logWrite("Pilot status = " + pilotStatus);
			
		}else if(parts[0].equals("STAT,ID")){
			parts = response.split(",");
			String id=null, aq=null, sv=null, pr=null;
			for(String part : parts){
				String keyValue[] = part.split("=",2);
				if(keyValue[0].equals("ID"))
					id = keyValue[1].trim();
				if(keyValue[0].equals("AQ"))
					aq = keyValue[1].trim();
				if(keyValue[0].equals("SV"))
					sv = keyValue[1].trim();
				if(keyValue[0].equals("PR"))
					pr = keyValue[1].trim();
			}
			
			ProgramStatus progStat = null;
			for(ProgramStatus stat : progStats)
				if(id.equals(stat.id)){
					progStat = stat;
					break;
				}
			
			if(progStat == null){
				progStat = new ProgramStatus(id);
				progStats.add(progStat);
			}

			progStat.acquireStatus = StepStatus.fromCode(aq.charAt(0));
			progStat.saveStatus = StepStatus.fromCode(sv.charAt(0));
			progStat.processStatus = StepStatus.fromCode(pr.charAt(0));
			
			logWrite("Program status " + progStat);
			
			
		} else if(parts[0].equals("GERISTATE")) {
			parts = parts[1].split(",");
			if(parts.length < 2)
				return;
			geriState = -1;
			geriStateChanged = -1;
			try {
				geriState = Integer.parseInt(parts[0]);
				geriStateChanged = Long.parseLong(parts[1]);
			}catch(NumberFormatException err) { }
			logWrite("GERI state changed to " + geriState + " at " + geriStateChanged);
			
			//broadcast to others
			ctrl.geriStateChange(geriState, geriStateChanged);
			
		} else {
			logWrite("Unprocessed: " + response);
		}
	}
	
	public void logWrite(String str) {
		Calendar now = Calendar.getInstance();
		String timeStamp = timestampFormat.format(now.getTime());
		String lineStr = timeStamp + ": " + str + "\n";
		
		log.append(lineStr);
	}
	
	public String getLog(){ return log.toString(); }

	/** @return the self-named 'id' of the pilot, maybe not unique */ 
	public String getSystemID() { return id; }
	
	public String getHost() { return host; }

	public int getPort() { return port;	} 
	
	public String getStatus(){ 
		if(thread == null || !thread.isAlive())
			return "Inactive";
		 
		if(socket == null || !socket.isConnected())
			return "Disconnected";
		
		return "Connected";
	}
	
	public StepStatus getPilotStatus() {
		return pilotStatus;
	}
	
	public ArrayList<ProgramStatus> getProgStats() {
		return progStats;
	}
	
	public void forceProgram(String progName, String startTime) {
		if(thread == null || !thread.isAlive())
			return;
		
		commands.add("forceProgram=" + progName + " " + startTime);
		thread.interrupt();
	}

	public boolean isConnected() {
		return (thread != null && thread.isAlive() && socket != null && socket.isConnected());
	}

	public void requestPilotLog() {
		//request a new log
		commands.add("readLog");
		thread.interrupt();
	}
	
	public String getPilotLog() { return lastPilotLog; }
	public Instant getPilotLogTime() { return lastPilotLogTime; }
	
	public void requestJavaLog() {
		//request a new log
		commands.add("readJavaLog");
		thread.interrupt();		
	}
	
	public String getJavaLog() { return lastJavaLog; }
	public Instant getJavaLogTime() { return lastJavaLogTime; }

	public void sendGeriStateChange(int state, long timestamp) {
		commands.add("geriState=" + state + "," + timestamp);
		thread.interrupt();		
	}
	
	public void setConfigParam(String param, String value) {
		commands.add("setConfigParam "+param+"="+value);
		thread.interrupt();
	}
	
	public void sendCommand(String command) {
		commands.add(command);
		thread.interrupt();		
	}
	
	public void setHost(String host) { this.host = host; }
	public void setPort(int port) { this.port = port; }
	public void setSystemID(String id) { this.id = id; }

}
